# Doodle chat
#### Code test by Viktor Lopatin

The coding task was implemented with Java base on SpringBoot, WebSockets and an in-memory H2 database.
The frontend side is in vanilla JS. To run the application, use `mvn spring-boot:run`, or use the packaged jar executable with `java -jar doodle-chat-1.0-SNAPSHOT.jar`.

The application is accessible via port 8080, is a SPA with the following functionality:

* Enter your name
* Enter the chat, see all previous messages
* Send and receive new messages

Should be working on all common browsers, desktop and mobile. Except IE, because of the ES6 support. But I hope that the reviewers would not consider IE a modern browser.

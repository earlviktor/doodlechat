package doodlechat.controller;

import doodlechat.model.Message;
import doodlechat.repository.MessageRepository;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;

@Controller
public class ChatController {

    MessageRepository repository;

    public ChatController(MessageRepository repository) {
        this.repository = repository;
    }

    @MessageMapping("/chat.sendMessage")
    @SendTo("/room/public")
    public Message sendMessage(@Payload Message message) {
        repository.save(message);
        return message;
    }

    @MessageMapping("/chat.addUser")
    @SendTo("/room/public")
    public Message addUser(@Payload Message message, SimpMessageHeaderAccessor headerAccessor) {
        headerAccessor.getSessionAttributes().put("user", message.getUser());
        repository.save(message);
        return message;
    }
}

package doodlechat.controller;

import doodlechat.model.Message;
import doodlechat.repository.MessageRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ApiController {

    private MessageRepository repository;

    public ApiController(MessageRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(method = RequestMethod.GET, path = "/history")
    public List<Message> getMessageHistory() {
        return repository.findAll();
    }
}

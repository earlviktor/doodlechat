class Chat {
    constructor() {
        this.usernamePage = document.querySelector('#username-page');
        this.chatPage = document.querySelector('#chat-page');
        this.usernameForm = document.querySelector('#usernameForm');
        this.messageForm = document.querySelector('#messageForm');
        this.messageInput = document.querySelector('#message');
        this.messageArea = document.querySelector('#messageArea');
        this.connectingElement = document.querySelector('.connecting');
        this.userAvatarElement = document.querySelector("#user-avatar");

        this.stompClient = null;
        this.username = null;

        this.colors = [
            '#2196F3', '#32c787', '#00BCD4', '#ff5652',
            '#ffc107', '#ff85af', '#FF9800', '#39bbb0'
        ];

        const self = this;

        this.usernameForm.addEventListener('submit', (e) => { self.connect(e) }, true);
        this.messageForm.addEventListener('submit', (e) => { self.sendMessage(e) }, true);

        this.loadHistory();
    }

    connect(event) {
        this.username = document.querySelector('#name').value.trim();

        if(this.username) {

            const avatarElement = document.createElement('span');
            const avatarText = document.createTextNode(this.username[0]);
            avatarElement.appendChild(avatarText);
            avatarElement.style['background-color'] = this.getAvatarColor(this.username);
            this.userAvatarElement.appendChild(avatarElement);

            this.usernamePage.classList.add('hidden');
            this.chatPage.classList.remove('hidden');

            const socket = new SockJS('/ws');
            this.stompClient = Stomp.over(socket);
            const self = this;

            this.stompClient.connect({}, () => { self.onConnected() }, () => { self.onError() });
        }
        event.preventDefault();
    }

    onConnected() {
        const self = this;
        // Subscribe to the Public Topic
        this.stompClient.subscribe('/room/public', (payload) => {self.onMessageReceived(payload) });

        // Tell your username to the server
        this.stompClient.send("/app/chat.addUser",
            {},
            JSON.stringify({user: this.username, type: 'JOIN'})
        );

        this.connectingElement.classList.add('hidden');
    }

    onError(error) {
        this.connectingElement.textContent = 'Could not connect to WebSocket server. Please refresh this page to try again!';
        this.connectingElement.style.color = 'red';
    }

    sendMessage(event) {
        const messageContent = this.messageInput.value.trim();
        if(messageContent && this.stompClient) {
            const chatMessage = {
                user: this.username,
                content: this.messageInput.value,
                type: 'CHAT'
            };
            this.stompClient.send("/app/chat.sendMessage", {}, JSON.stringify(chatMessage));
            this.messageInput.value = '';
        }
        event.preventDefault();
    }

    onMessageReceived(payload) {
        this.processMessage(JSON.parse(payload.body));
    }

    processMessage(message) {
        const messageElement = document.createElement('li');
        if(message.type === 'JOIN') {
            messageElement.classList.add('event-message');
            message.content = message.user + ' joined!';
        } else if (message.type === 'LEAVE') {
            messageElement.classList.add('event-message');
            message.content = message.user + ' left!';
        } else {
            messageElement.classList.add('chat-message');

            const avatarElement = document.createElement('i');
            const avatarText = document.createTextNode(message.user[0]);
            avatarElement.appendChild(avatarText);
            avatarElement.style['background-color'] = this.getAvatarColor(message.user);

            messageElement.appendChild(avatarElement);

            const usernameElement = document.createElement('span');
            const usernameText = document.createTextNode(message.user);
            usernameElement.appendChild(usernameText);
            messageElement.appendChild(usernameElement);
        }

        var textElement = document.createElement('p');
        var messageText = document.createTextNode(message.content);
        textElement.appendChild(messageText);

        messageElement.appendChild(textElement);

        this.messageArea.appendChild(messageElement);
        this.messageArea.scrollTop = this.messageArea.scrollHeight;
    }

    loadHistory() {
        const xhr = new XMLHttpRequest();
        const self = this;
        xhr.open("GET", "/history");
        xhr.onload = () => {
            if (xhr.status === 200) {
                const data = JSON.parse(xhr.responseText);
                for (let i = 0; i < data.length; i++) {
                    self.processMessage(data[i]);
                }
            }
        };
        xhr.send();
    }


    getAvatarColor(messageSender) {
        let hash = 0;
        for (let i = 0; i < messageSender.length; i++) {
            hash = 31 * hash + messageSender.charCodeAt(i);
        }
        const index = Math.abs(hash % this.colors.length);
        return this.colors[index];
    }

}

export default Chat;